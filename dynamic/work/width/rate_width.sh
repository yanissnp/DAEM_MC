rm *.out

for time in {700..2200..10}
do 
	for largeur in 5000 10000 15000
	do 
		../../build/cmc_dynam -n 1 -A 1e14 -t $time -T 443 -w $largeur -d >> deter_$largeur.out 
	done 
done 

for time in {700..2200..20}
do
	for largeur in 5000 10000 15000
	do 
		../../build/cmc_dynam -n 40000 -A 1e14 -t $time -T 443 -w $largeur  >> mc_$largeur.out 
	done
done
