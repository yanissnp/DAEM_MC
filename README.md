# DAEM_MC
`DAEM_MC` is a Monte-Carlo estimator of the reaction rate based on the DAEM (Distributed 
Activation Energy Model). The project aim to reproduce the results of the article: A Monte-Carlo Based Strategy to Assess Complex Kinetics: Application of the Null-Reaction Method to DAEM" 

The folder dynamic is for the non-isotherm results.
The folder isotherm is for the isotherm results.
The folder Miura is for the MC estimation of the results from: "Miura, K.; Maki, T. A Simple Method for Estimating f(E) and k0( E ) in the Distributed Activation Energy Model. Energy & Fuels 1998 , 12 , 864869". 
Every software relies on the [CMake](http://www.cmake.org) and the
[RCMake](https://gitlab.com/vaplv/rcmake/) VERSION 0.4 packages to build. It also depends
on the [GSL](https://www.gnu.org/software/gsl/) VERSION 2.7,
[RSys](https://gitlab.com/vaplv/rsys/) VERSION  0.8.1,
[Star-SP](https://gitlab.com/meso-star/stat-sp/) VERSION 0.8.1  and
[Star-MC](https://gitlab.com/meso-star/stat-mc/) VERSION 0.4 libraries. It is compatible
GNU/Linux on x86-64 architectures.

## Copyright notice
Copyright (C) 2021 Université Paul Sabatier (<yaniss.nyffenegger-pere@laplace.univ-tlse.fr>).  


## License
`DAEM_MC` is free software released under the GPL v3+ license: GNU GPL
version 3 or later. You are welcome to redistribute it under certain
conditions; refer to the COPYING file for details.
