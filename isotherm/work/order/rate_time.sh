rm *.out

for order in 2.5 1 1.5 2
	do
	../build/cmc_iso -n 1 -A 1e14 -t 0.0001 -T 773 -w 30000 -d -o $order >> deter_$order.out 
done

for time in {1..150..1}
do
	for order in 2.5 1 1.5 2
	do
	../build/cmc_iso -n 1 -A 1e14 -t $time -T 773 -w 30000 -d -o $order >> deter_$order.out 
	done
done

for time in {1..150..5}
do
	for order in 2.5 1 1.5 2
	do
	../build/cmc_iso -n 100000 -A 1e14 -t $time -T 773 -w 30000  -o $order >> mc_$order.out 
	done
done
