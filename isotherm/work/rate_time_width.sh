rm *.out

for time in {1..60..1}
do
	for width in 1000 5000 30000 
	do
	../../build/cmc_iso -n 1 -A 1e14 -t $time -T 773 -w $width -d -o 1 >> deter_$width.out 
	done
done

for time in {1..60..2}
do
	for width in 1000 5000 30000 
	do
	../../build/cmc_iso -n 100000 -A 1e14 -t $time -T 773 -w $width  -o 1 >> mc_$width.out 
	done
done
