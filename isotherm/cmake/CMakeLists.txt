# Copyright (C) 2021 UPS (yaniss.nyffenegger-pere@laplace.univ-tlse.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

cmake_minimum_required(VERSION 3.12)
project(cmc_iso)
enable_language(C)
set(CMC_ISO_SOURCE_DIR ${PROJECT_SOURCE_DIR}/../src)

################################################################################
# Check dependencies
################################################################################
find_package(RCMake 0.3 REQUIRED)
find_package(RSys 0.6 REQUIRED)
find_package(StarSP 0.7 REQUIRED)
find_package(StarMC 0.4 REQUIRED)

include_directories(
  ${RSys_INCLUDE_DIR}
  ${StarSP_INCLUDE_DIR}
  ${StarMC_INCLUDE_DIR})

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${RCMAKE_SOURCE_DIR})
include(rcmake)
include(rcmake_runtime)

################################################################################
# Configure and define the targets
################################################################################
set(VERSION_MAJOR 0)
set(VERSION_MINOR 0)
set(VERSION_PATCH 1)
set(VERSION ${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH})

# CMC_ISO
set(CMC_ISO_FILES_SRC 
  cmc_iso.c
  cmc_iso_args.c
  cmc_iso_main.c
  cmc_iso_compute_rate.c
  cmc_iso_log.c)

set(CMC_ISO_FILES_INC   
  cmc_iso.h
  cmc_iso_args.h
	cmc_iso_compute_rate.h
  cmc_iso_log.h)

# Prepend each file in the `CMC_ISO_FILES_<SRC|INC>' list by the absolute
# path of the directory in which they lie
rcmake_prepend_path(CMC_ISO_FILES_SRC ${CMC_ISO_SOURCE_DIR})
rcmake_prepend_path(CMC_ISO_FILES_INC ${CMC_ISO_SOURCE_DIR})

add_executable(cmc_iso ${CMC_ISO_FILES_SRC} ${CMC_ISO_FILES_INC})
target_link_libraries(cmc_iso PRIVATE
	RSys 
	StarSP 
	StarMC 
	gsl
	m)

