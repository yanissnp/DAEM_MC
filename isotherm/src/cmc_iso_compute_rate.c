/* Copyright (C) 2021 UPS (yaniss.nyffenegger-pere@laplace.univ-tlse.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "cmc_iso.h"
#include "cmc_iso_compute_rate.h"

#include <star/ssp.h>
#include <star/smc.h>

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
daem_realization
	(void* length, 
	 struct ssp_rng* rng, 
	 const unsigned ithread, 
	 void* context)
{
  struct cmc_iso* cmc_iso = (struct cmc_iso*)context;
  double weigth;
  double energy;
  const double R = 8.314;

	/*Gaussian distribution energy sampling */
  energy = ssp_ran_gaussian(rng, cmc_iso->mean_energy, cmc_iso->width_gauss);

	/*Monte-Carlo weigth */
	if(cmc_iso->order == 1) {
  	weigth = exp(- cmc_iso->prexponential_factor 
					 * exp(-energy/(R*cmc_iso->init_temperature)) 
					 * cmc_iso->final_time);
	} else {
		weigth = pow(1.0 - (1.0-cmc_iso->order)*cmc_iso->prexponential_factor* 
						 exp(-energy/(R*cmc_iso->init_temperature))*cmc_iso->final_time, 1.0/(1.0-cmc_iso->order));
	}

  SMC_DOUBLE(length) = weigth;
  return RES_OK;
}
