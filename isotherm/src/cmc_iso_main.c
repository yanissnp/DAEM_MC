/* Copyright (C) 2021 UPS (yaniss.nyffenegger-pere@laplace.univ-tlse.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "cmc_iso.h"
#include "cmc_iso_args.h"

/*******************************************************************************
 * Program
 ******************************************************************************/
int
main(int argc, char** argv)
{
  struct cmc_iso cmc_iso;
  struct cmc_iso_args args = CMC_ISO_ARGS_DEFAULT;
  int err = 0;
  res_T res = RES_OK;

  /* Get argument values */
  res = cmc_iso_args_init(&args, argc, argv);
  if(res != RES_OK) goto error;
	if(args.quit == 1) goto exit;

  /* Initialize global context */
  res = cmc_iso_init(&args, &cmc_iso);
  if(res != RES_OK) goto error;

  /* Run project */
  res = cmc_iso_run(&cmc_iso);
  if(res != RES_OK) goto error;

exit:
  return err;
error:
  err = -1;
  printf("Error occured !\n");
  goto exit;
}
