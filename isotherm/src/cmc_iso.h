/* Copyright (C) 2021 UPS (yaniss.nyffenegger-pere@laplace.univ-tlse.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef CMC_ISO_H
#define CMC_ISO_H

#include <rsys/logger.h>
#include <rsys/ref_count.h>
#include <rsys/str.h>

/* Forward declarations */
struct cmc_iso_args;

struct cmc_iso{
	size_t nrealisations;
  int deter; 
	double init_temperature;
	double mean_energy; 
	double width_gauss; 
	double prexponential_factor;
	double final_time; 
	double order;

	struct smc_device* smc;
  unsigned nthreads;
  struct logger logger;

};

extern LOCAL_SYM res_T
cmc_iso_init
  (const struct cmc_iso_args* args,
   struct cmc_iso* cmc_iso);

extern LOCAL_SYM void
cmc_iso_release
  (struct cmc_iso* cmc_iso);

extern LOCAL_SYM res_T
cmc_iso_run
  (struct cmc_iso* cmc_iso);

extern LOCAL_SYM void
setup_logger
  (struct cmc_iso* cmc_iso);

static FINLINE unsigned
cmc_iso_get_threads_count(const struct cmc_iso* cmc_iso)
{
  ASSERT(cmc_iso);
  return cmc_iso->nthreads;
}

#endif /* CMC_ISO_H */
