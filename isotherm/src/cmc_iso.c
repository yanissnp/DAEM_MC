/* Copyright (C) 2021 UPS (yaniss.nyffenegger-pere@laplace.univ-tlse.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#include <star/ssp.h>       
#include <star/smc.h>       

#include <gsl/gsl_integration.h>

#include "cmc_iso.h"
#include "cmc_iso_args.h"
#include "cmc_iso_compute_rate.h"
#include "cmc_iso_log.h"

#include <rsys/clock_time.h>
#include <rsys/cstr.h>
#include <rsys/mutex.h>
#include <rsys/str.h>

/*******************************************************************************
 * Helpers functions
 ******************************************************************************/
/* Function for Gauss-Hermite determinist calculus */
static double
one_minus_alpha
  (double a,
   void *params)
{
  const double R = 8.314;
  double *values = (double*) params;
  double value_stat;
  value_stat = values[0] * values[4] * exp(-(values[1] + a*sqrt(2)*values[2] ) / (R*values[3]));
	if(values[5] == 1){
  	return 1.0/sqrt(PI) * exp(-value_stat);
	}else{
		return 1.0/sqrt(PI) * pow(1.0-(1.0-values[5]) * value_stat, 1.0/(1.0-values[5]));
	}
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
cmc_iso_init
  (const struct cmc_iso_args* args,
   struct cmc_iso* cmc_iso)
{
  res_T res = RES_OK;
  ASSERT(args && cmc_iso);

  memset(cmc_iso, 0, sizeof(*cmc_iso)); /* Reset memory */

  setup_logger(cmc_iso);

	/* Initialize global parameters */
  cmc_iso->nrealisations = args->nrealisations;
  cmc_iso->deter = (int)args->deter;
  cmc_iso->init_temperature = args->init_temperature;
  cmc_iso->mean_energy = args->mean_energy;
  cmc_iso->width_gauss = args->width_gauss;
  cmc_iso->prexponential_factor = args->prexponential_factor;
  cmc_iso->final_time = args->final_time;
  cmc_iso->order = args->order;

  /* Create the Star-MonteCarlo device */
  res = smc_device_create
    (&cmc_iso->logger, NULL, SMC_NTHREADS_DEFAULT, NULL, &cmc_iso->smc);
  if(res != RES_OK) {
    cmc_iso_log_err(cmc_iso,
      "Could not create the Star-MonteCarlo device -- %s.\n",
      res_to_cstr(res));
    goto error;
  }
  SMC(device_get_threads_count(cmc_iso->smc, &cmc_iso->nthreads));

	/* Shooting values */
  cmc_iso_log(cmc_iso, "Reaction order: %g\n", cmc_iso->order);
  cmc_iso_log(cmc_iso, "Time of interest: %g seconds\n", cmc_iso->final_time);
  cmc_iso_log(cmc_iso, "Initial temperature : %g Kelvin\n", cmc_iso->init_temperature);
  cmc_iso_log(cmc_iso, "Gaussian mean energy: %g\n", cmc_iso->mean_energy);
  cmc_iso_log(cmc_iso, "Gaussian half width : %g\n", cmc_iso->width_gauss);
  cmc_iso_log(cmc_iso, "Pre-exponential factor: %g\n", cmc_iso->prexponential_factor);
  cmc_iso_log(cmc_iso, "#realisations: %ld\n", cmc_iso->nrealisations);

exit:
  return res;
error:
  cmc_iso_release(cmc_iso);
  goto exit;
}

void
cmc_iso_release(struct cmc_iso* cmc_iso)
{
  ASSERT(cmc_iso);
  logger_release(&cmc_iso->logger);
}

res_T
cmc_iso_run(struct cmc_iso* cmc_iso)
{
  struct smc_integrator integrator;
  struct smc_estimator* estimator = NULL;
  struct smc_estimator_status estimator_status;
  struct time t0, t1;
  char dump[64];
  res_T res = RES_OK;

	/* Setup Star-MC */
	integrator.integrand = &daem_realization; /* Realization function */
	integrator.type = &smc_double; /* Type of the Monte Carlo weight */
	integrator.max_realisations = cmc_iso->nrealisations; /* Realization count */
	integrator.max_failures = cmc_iso->nrealisations / 1000;
  time_current(&t0);
	
	/* Monte-Carlo estimation */
	SMC(solve(cmc_iso->smc, &integrator, cmc_iso, &estimator));
	
	/* Print the simulation results */
	SMC(estimator_get_status(estimator, &estimator_status));
  cmc_iso_log(cmc_iso, "Monte-Carlo estimation ");
	printf("%g %g %g",
		cmc_iso->final_time, SMC_DOUBLE(estimator_status.E),SMC_DOUBLE(estimator_status.SE)); 

	/* Gauss-Hermite estimation */
	if(cmc_iso->deter == 1){
		int err = 0;
		size_t n;
		double param_func[6];
		double result;
		const gsl_integration_fixed_type * T = gsl_integration_fixed_hermite;
		gsl_integration_fixed_workspace * w;
		gsl_function F;

  	cmc_iso_log(cmc_iso, "With determinist calculus\n");
		/* Finite-difference resolution 
		* GSL parameter */
		param_func[0] = cmc_iso->prexponential_factor;
		param_func[1] = cmc_iso->mean_energy;
		param_func[2] = cmc_iso->width_gauss;
		param_func[3] = cmc_iso->init_temperature;
		param_func[4] = cmc_iso->final_time;
		param_func[5] = cmc_iso->order;
		
		/* GSL allocation */
		n = 40;
		w = gsl_integration_fixed_alloc(T, n, 0.0, 1.0, 0.0, 0.0);
		F.function = &one_minus_alpha;

		/* Solve integrand with gauss quadrature */
		F.params = &param_func;
		err = gsl_integration_fixed(&F, &result, w);
		if(err == 1){
			res = RES_BAD_ARG;
			goto error;
		}
		printf (" %g ", result);
  	gsl_integration_fixed_free(w);
	}
	
	time_sub(&t0, time_current(&t1), &t0);
	time_dump(&t0, TIME_ALL, NULL, dump, sizeof(dump));
	printf(" Computation time: %s\n", dump);

exit:
  return res;
error:
  cmc_iso_release(cmc_iso);
  goto exit;
}
