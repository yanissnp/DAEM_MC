/* Copyright (C) 2021 UPS (yaniss.nyffenegger-pere@laplace.univ-tlse.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 2 /* strtok_r support */

#include "cmc_dynam_args.h"

#include <rsys/cstr.h>
#include <rsys/double3.h>

#include <getopt.h>
#include <string.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
print_help(const char* cmd)
{
  ASSERT(cmd);
  ASSERT(CMC_DYNAM_ARGS_DEFAULT.deter == 0); /* Check default values */

  printf(
"Usage: cmc_dynam -t <time of interest> -m <miura file> [OPTIONS]...\n");

  printf("\n");
  printf(
"Estimate the conversion rate at a specific time.\n\n");
  printf(
"  -d             Compute in parallel the determinist mode.\n"
"                 Disabled by default.\n");
  printf(
"  -h             display this help and exit.\n");
	  printf(
"  -m <miura file>   path of the miura energy distribution density.\n");
  printf(
"  -n <nrealisations>\n"
"                 number of Monte-Carlo realisation\n"
"                 (default: %lu)\n",
    (unsigned long)CMC_DYNAM_ARGS_DEFAULT.nrealisations);
  printf(
"  -T <initial temperature>  \n"
"                 (default: %g)\n",
    CMC_DYNAM_ARGS_DEFAULT.init_temperature);

  printf("\n");
  printf(
"Cmc_iso is a free software released under the GNU GPL license,\n"
"version 3 or later. You are free to change or redistribute it\n"
"under certain conditions <http://gnu.org/licenses/gpl.html>.\n");
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
cmc_dynam_args_init(struct cmc_dynam_args* args, int argc, char** argv)
{
  int opt;
  res_T res = RES_OK;
  ASSERT(args && argc && argv);

  *args = CMC_DYNAM_ARGS_DEFAULT;
  while((opt = getopt(argc, argv, "T:t:m:dhn:w:")) != -1) {
    switch(opt) {
      case 'd': args->deter = 1; break;
      case 'h':
        print_help(argv[0]);
        cmc_dynam_args_release(args);
        args->quit = 1;
        goto exit;
      case 'n': /* Number of Monte_Carlo realisation*/
        res = cstr_to_uint(optarg, &args->nrealisations);
        if(res == RES_OK && !args->nrealisations) res = RES_BAD_ARG;
        break;
		case 'T': /* Initial temperature */
        res = cstr_to_double(optarg, &args->init_temperature);
        if(res == RES_OK && !args->nrealisations) res = RES_BAD_ARG;
        break;
		case 't': /* Time of interest */
        res = cstr_to_double(optarg, &args->final_time);
        if(res == RES_OK && !args->nrealisations) res = RES_BAD_ARG;
        break;
		case 'm': args->path_miura_file = optarg; break;
      default: res = RES_BAD_ARG; break;
    }
    if(res != RES_OK) {
      if(optarg) {
        fprintf(stderr, "%s: invalid option argument '%s' -- '%c'\n",
          argv[0], optarg, opt);
      }
      goto error;
    }
  }
	
	if(!args->path_miura_file) {
    fprintf(stderr, "Missing the miura distribution file path -- option '-m'\n");
    res = RES_BAD_ARG;
    goto error;
  }

exit:
  return res;
error:
  cmc_dynam_args_release(args);
  goto exit;
}

void
cmc_dynam_args_release(struct cmc_dynam_args* args)
{
  ASSERT(args);
  *args = CMC_DYNAM_ARGS_DEFAULT;
}
