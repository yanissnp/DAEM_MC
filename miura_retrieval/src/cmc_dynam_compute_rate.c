/* Copyright (C) 2021 UPS (yaniss.nyffenegger-pere@laplace.univ-tlse.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "cmc_dynam.h"
#include "cmc_dynam_log.h"
#include "cmc_dynam_distribution.h"
#include "cmc_dynam_compute_rate.h"

#include <star/ssp.h>
#include <star/smc.h>

/*******************************************************************************
 * Helper function
 ******************************************************************************/

/* Altitude interpolation */
static double
value_interpolation
	(struct cmc_dynam* cmc_dynam,
	int i,
	double r)
{
  double A, B;
  double value_inter;
  double cdf_min, cdf_max;
  double energy_min, energy_max;
  ASSERT(cmc_dynam);

	cdf_min = cmc_dynam->cumulative[i-1]; 
	cdf_max = cmc_dynam->cumulative[i]; 
	energy_min = cmc_dynam_distrib_get_energy(cmc_dynam->distrib)[i-1];
	energy_max = cmc_dynam_distrib_get_energy(cmc_dynam->distrib)[i];

  A = (cdf_min - cdf_max) / (energy_min - energy_max);
  B = (energy_min*cdf_max - energy_max*cdf_min) / (energy_min - energy_max);
  value_inter = (r-B)/A;
  return value_inter;
}



/* Sample an energy from the miura distribution */
static double
sample_energy
	(struct cmc_dynam* cmc_dynam,
   struct ssp_rng* rng)
{
	int i, number_values;
	double energy;
	double r;
	number_values = cmc_dynam_distrib_get_count(cmc_dynam->distrib); 
  r = ssp_rng_canonical(rng);
	energy = 0.0;
	FOR_EACH(i, 0, number_values+1) {
		if( r < cmc_dynam->cumulative[i]) {
			/*energy = cmc_dynam_distrib_get_energy(cmc_dynam->distrib)[i];*/
			energy = value_interpolation(cmc_dynam, i, r);
			break;
		}
		/* TODO gerer la borne max -> 1*/
	}
	return energy;
}

/* Extract temperature for a time given*/
static double
temperature
	(double time,
   struct cmc_dynam* cmc_dynam)
{
  return time*cmc_dynam->beta + cmc_dynam->init_temperature;
}

/* Arrenhius function */
static double
K
  (struct cmc_dynam* cmc_dynam,
   double energy,
   double time)
{
  const double R = 8.314;
	double prexponential_factor;
	
	/* From miura Energy & Fuels 1998, 12, 864-869 */
	prexponential_factor = 1e15*exp(0.08*(1e-3*energy-250)); 
  return prexponential_factor * exp(-energy/(R*temperature(time, cmc_dynam)));
}

/* Check if a reaction occured in final_time */
static double
time_path
  (double energy,
   struct ssp_rng* rng,
   struct cmc_dynam* cmc_dynam)
{
  double time = 0.0;
  double weigth, t_samp;

  for(;;){
    double r;
    double proba;
    double dhat;
    double dtime;

    /* Time sampling */

    if( time < cmc_dynam->final_time/3){
      dhat = K(cmc_dynam, energy, cmc_dynam->final_time/3);
      t_samp = ssp_ran_exp(rng, dhat);
      time += t_samp;

      if(time > cmc_dynam->final_time/3){
        time = cmc_dynam->final_time/3;
        continue;
      }
    }

    if( time < cmc_dynam->final_time/2 && time >= cmc_dynam->final_time/3){
      dhat = K(cmc_dynam, energy, cmc_dynam->final_time/2);
      t_samp = ssp_ran_exp(rng, dhat);
      time += t_samp;

      if(time > cmc_dynam->final_time/2){
        time = cmc_dynam->final_time/2;
        continue;
      }
    }

    if(time >= cmc_dynam->final_time/2){
      dhat = K(cmc_dynam, energy, cmc_dynam->final_time);
      t_samp = ssp_ran_exp(rng, dhat);
      time += t_samp;

      if(time > cmc_dynam->final_time){
        weigth = 1.0;
        break;
      }

    }

    /* Reaction occured ? */
    r = ssp_rng_canonical(rng);
    dtime = K(cmc_dynam, energy, time);
    proba = dtime/dhat;

    if(proba > 1.0001){
      printf("proba sup 1 %g %g %g \n", dtime, dhat, time);
      goto error;
    }
    else if(r < proba){
      weigth = 0;
      break;
    }
  }
  return weigth;

error :
  return -1;
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
daem_realization
	(void* length, 
	 struct ssp_rng* rng, 
	 const unsigned ithread, 
	 void* context)
{
  struct cmc_dynam* cmc_dynam = (struct cmc_dynam*)context;
  double weigth;
  double energy;
  res_T res = RES_OK;

  /* Distribution energy sampling */
  energy = 1e3*sample_energy(cmc_dynam, rng);
	if(energy == 0){
		cmc_dynam_log(cmc_dynam, "error");
    goto error;
  }

	/* Reaction occured ? */
  weigth = time_path(energy, rng, cmc_dynam);
  if(weigth == -1){
    goto error;
  }

exit :
  SMC_DOUBLE(length) = weigth;
  return res;
error:
  res = RES_BAD_ARG;
  goto exit;
}
