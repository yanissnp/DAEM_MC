/* Copyright (C) 2021 UPS (yaniss.nyffenegger-pere@laplace.univ-tlse.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef CMC_DYNAM_ARGS_H
#define CMC_DYNAM_ARGS_H

#include <float.h>
#include <limits.h>
#include <rsys/rsys.h>

struct cmc_dynam_args {
  const char* path_miura_file;

  unsigned int nrealisations; /* #samples */
  double init_temperature; /* Initial temperature in Kelvin */
  double mean_energy; /* Mean energy for gaussian distribution */
  double width_gauss; /* mean width for gaussian distribution */
  double final_time; /* time of interest in second(s)*/
  unsigned int deter; /* Enable derterminist calculus */
  int quit; /* Quit the application */
};

#define CMC_DYNAM_ARGS_DEFAULT__ {                                    \
	NULL, /* Miura data */																							\
  10000, /* #samples */                                               \
  443, /* initial temperature */                                      \
  220000, /* Mean energy */                                           \
  3000, /* Mean width */                                              \
  1500, /* time interest */                                           \
  0, /* Determinist calculus */                                       \
  0, /* Quit the application */                                       \
}
static const struct cmc_dynam_args CMC_DYNAM_ARGS_DEFAULT = CMC_DYNAM_ARGS_DEFAULT__;

extern LOCAL_SYM res_T
cmc_dynam_args_init
  (struct cmc_dynam_args* args,
   int argc,
   char** argv);

extern LOCAL_SYM void
cmc_dynam_args_release
  (struct cmc_dynam_args* args);

#endif /* CMC_DYNAM_ARGS_H */
