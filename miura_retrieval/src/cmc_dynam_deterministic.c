/* Copyright (C) 2021 UPS (yaniss.nyffenegger-pere@laplace.univ-tlse.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "cmc_dynam.h"
#include "cmc_dynam_log.h"
#include "cmc_dynam_args.h"
#include "cmc_dynam_deterministic.h"

#include <gsl/gsl_integration.h>

/*******************************************************************************
 * Helper function
 ******************************************************************************/

/* TIME FUNCTION */
static double
time_integral
  (double time,
   void *params)
{
  double *values = (double*) params;
  const double R = 8.314;
  double value_stat;
  double A, sigma, mean_energy;
  double energy, temperature_init;
  double beta, temperature;

  mean_energy = values[1];
  sigma = values[2];
  energy = values[7];
  temperature_init = values[3];
  beta = values[8];
  temperature = beta*time + temperature_init;
  A = 1e15*exp(0.08*(energy-250)); 
  value_stat = A * exp( -(sigma*sqrt(2)*energy + mean_energy) / (R*temperature));
  return value_stat;
}

/* ENERGY FUNCTION */
static double
one_minus_alpha
  (double energy,
   void *params)
{
  int err = 0;
  double *values = (double*) params;
  double result_time;
  double error;
  double time;
  gsl_integration_workspace * w  = gsl_integration_workspace_alloc (2000);
  gsl_function F;

	/* Local variable */
  values[7] = energy;
  time = values[4];

  /*TIME INTEGRAL QUADRATURE */
  F.function = &time_integral;
  F.params = values;
  err = gsl_integration_qags(&F, 0, time, 0, 1e-7, 2000,
                        w, &result_time, &error);
	if(err == 1){
		printf("Error while calculate quadrature integration \n");  
	}

  return 1.0/sqrt(3.14159265) * exp(-result_time);
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
deter
	(struct cmc_dynam* cmc_dynam,
	 double *result)
{
  int err = 0;
  size_t n;
  double result_energy;
  double param_func[9];
  const gsl_integration_fixed_type * T = gsl_integration_fixed_hermite;
  gsl_function F_energy;
	gsl_integration_fixed_workspace * w;
	res_T res = RES_OK;

  /* GSL PARMAMETERS FOR TIME INTEGRATION*/ 
  param_func[3] = cmc_dynam->init_temperature;      
  param_func[4] = cmc_dynam->final_time;   				  
  param_func[5] = 1;
  param_func[6] = 1;
  param_func[7] = 0.0;	
  param_func[8] = cmc_dynam->beta; 

  /* GSL PARMAMETERS FOR ENERGY INTEGRATION */
  n = 40;
  w = gsl_integration_fixed_alloc(T, n, 0.0, 1.0, 0.0, 0.0);
  F_energy.function = &one_minus_alpha;

  /* SOLVE INTEGRAND WITH GAUSS QUADRATURE */
  F_energy.params = &param_func;
  err = gsl_integration_fixed(&F_energy, &result_energy, w);
  if(err == 1) goto error;
	
	*result = result_energy;
  gsl_integration_fixed_free(w);

exit :
  return res;
error :
	cmc_dynam_log_err(cmc_dynam, "Error while calculate deterministic integration \n");
	res = RES_BAD_ARG;
  goto exit;
}

