/* Copyright (C) 2021 UPS (yaniss.nyffenegger-pere@laplace.univ-tlse.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef CMC_DYNAM_H
#define CMC_DYNAM_H

#include <rsys/logger.h>
#include <rsys/ref_count.h>
#include <rsys/str.h>

/* Forward declarations */
struct cmc_dynam_args;
struct cmc_dynam_distrib;

struct cmc_dynam{
	size_t nrealisations;
  int deter; 
  int index_realisation; 
	double init_temperature;
	double final_time; 
	double khat;
	double beta;
	double* cumulative;
  unsigned nthreads;
	struct smc_device* smc;
  struct logger logger;
	struct cmc_dynam_distrib* distrib;
};

extern LOCAL_SYM res_T
cmc_dynam_init
  (const struct cmc_dynam_args* args,
   struct cmc_dynam* cmc_dynam);

extern LOCAL_SYM void
cmc_dynam_release
  (struct cmc_dynam* cmc_dynam);

extern LOCAL_SYM res_T
cmc_dynam_run
  (struct cmc_dynam* cmc_dynam);

extern LOCAL_SYM void
setup_logger
  (struct cmc_dynam* cmc_dynam);

static FINLINE unsigned
cmc_dynam_get_threads_count(const struct cmc_dynam* cmc_dynam)
{
  ASSERT(cmc_dynam);
  return cmc_dynam->nthreads;
}

#endif /* CMC_DYNAM_H */
