/* Copyright (C) 2021 UPS (yaniss.nyffenegger-pere@laplace.univ-tlse.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef CMC_DYNAM_LOG_H
#define CMC_DYNAM_LOG_H

#include <rsys/rsys.h>

#define CMC_DYNAM_LOG_INFO_PREFIX "\x1b[1m\x1b[32m>\x1b[0m "
#define CMC_DYNAM_LOG_ERROR_PREFIX "\x1b[31merror:\x1b[0m "
#define CMC_DYNAM_LOG_WARNING_PREFIX "\x1b[33mwarning:\x1b[0m "

struct cmc_dynam;

extern LOCAL_SYM void
cmc_dynam_log
  (struct cmc_dynam* cmc_dynam,
   const char* msg,
   ...)
#ifdef COMPILER_GCC
  __attribute((format(printf, 2, 3)))
#endif
  ;

extern LOCAL_SYM void
cmc_dynam_log_err
  (struct cmc_dynam* cmc_dynam,
   const char* msg,
   ...)
#ifdef COMPILER_GCC
  __attribute((format(printf, 2, 3)))
#endif
  ;

extern LOCAL_SYM void
cmc_dynam_log_warn
  (struct cmc_dynam* cmc_dynam,
   const char* msg,
   ...)
#ifdef COMPILER_GCC
  __attribute((format(printf, 2, 3)))
#endif
  ;

#endif /* CMC_DYNAM_LOG_H */
